#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QFont>
#include <QPainterPath>

namespace Ui {
    class MainWindow;
    class PathAndBrush;
}

class PathAndBrush
{
public:
    QPainterPath path;
    QBrush brush;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_fontButton_clicked();

    void on_shadowColourButton_clicked();

    void on_gradientAngleBox_valueChanged(double arg1);

    void on_gradientAngleBox_editingFinished();

    void on_fontGradientEditor_gradientStopsChanged(const QGradientStops &stops);

    void on_dropShadowCheckbox_clicked();

    void on_strokeCheckbox_clicked();

    void on_dropShadowAngleBox_valueChanged(double arg1);

    void on_dropShadowSizeBox_valueChanged(double arg1);

    void on_shadowDistanceBox_valueChanged(double arg1);

    void on_strokeWidthBox_valueChanged(double arg1);

    void on_strokeGradientEditor_customContextMenuRequested(const QPoint &pos);

    void on_strokeGradientEditor_gradientStopsChanged(const QGradientStops &stops);

    void on_exportButton_clicked();

private:
    Ui::MainWindow *ui;
    QPushButton* _fontButton;
    void refreshFontButton();
    QFont _currentFont;
    QColor _shadowColour;
    QVector<PathAndBrush> getPathForCharacter(QChar pChar, QPointF baseline);
    void refreshPreview();
};

#endif // MAINWINDOW_H
