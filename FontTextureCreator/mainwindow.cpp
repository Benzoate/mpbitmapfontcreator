#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFontDialog>
#include <QColorDialog>
#include <QPainterPath>
#include <QPointF>
#include "pathstrokerenderer.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _fontButton = ui->fontButton;
    _currentFont = QFont("Sans Serif", 11);
    refreshFontButton();
    _shadowColour = QColor(0,0,0,255);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::refreshFontButton()
{
    QString fontString = _currentFont.family() + " %1";
    fontString = fontString.arg(_currentFont.pointSize());

    QFont buttonFont(_currentFont);
    buttonFont.setPointSizeF(_fontButton->font().pointSizeF());
    _fontButton->setFont(buttonFont);

    _fontButton->setText(fontString);
}

void MainWindow::on_fontButton_clicked()
{
    bool pressedOK = false;
    QFont newFont = QFontDialog::getFont(&pressedOK,_currentFont, this);
    if(pressedOK)
    {
        _currentFont = newFont;
        refreshFontButton();
    }
        refreshPreview();
}

void MainWindow::on_shadowColourButton_clicked()
{
    _shadowColour = QColorDialog::getColor(_shadowColour,this, "Shadow Color", QColorDialog::ShowAlphaChannel);
    refreshPreview();
}

QVector<PathAndBrush> MainWindow::getPathForCharacter(QChar pChar, QPointF baseline)
{
    QVector<PathAndBrush> retVec;

    QGradientStops stops = ui->fontGradientEditor->getGradientStops();
    QFontMetrics fontInfo(_currentFont);


    float radius = sqrt((float)(fontInfo.height()/2.0f*fontInfo.height()/2.0f)
                    + (float)(fontInfo.maxWidth()/2.0f * fontInfo.maxWidth()/2.0f));

    float fillAngle = ui->gradientAngleBox->value();
    float gradientOffsetX = sin(fillAngle * ((2.0f * 3.14159265f)/360.0f)) * radius;
    float gradientOffsetY = cos(fillAngle * ((2.0f * 3.14159265f)/360.0f)) * radius;


    //QPointF baseline(0, (fontInfo.height()+fontInfo.leading()));

    if(ui->dropShadowCheckbox->isChecked())
    {
        QPointF shadowBaseline(baseline);
        PathAndBrush shadow;
        float shadowAngle = ui->dropShadowAngleBox->value();
        float shadowDistance = ui->shadowDistanceBox->value();
        float shadowOffsetX = sin(shadowAngle * ((2.0f * 3.14159265f)/360.0f)) * shadowDistance;
        float shadowOffsetY = cos(shadowAngle * ((2.0f * 3.14159265f)/360.0f)) * shadowDistance;
        shadowBaseline.setX(shadowBaseline.x() + shadowOffsetX);
        shadowBaseline.setY(shadowBaseline.y() - shadowOffsetY);

        shadow.path.addText(shadowBaseline, _currentFont, QString(pChar));

        QPainterPathStroker stroker;
        stroker.setWidth(ui->dropShadowSizeBox->value());
        stroker.setJoinStyle(Qt::MiterJoin); // and other adjustments you need

        shadow.path += stroker.createStroke(shadow.path);
        shadow.brush = _shadowColour;
        retVec.append(shadow);
    }
    PathAndBrush mainFont;

    mainFont.path.addText(baseline, _currentFont, QString(pChar));

    float gradientStartX = baseline.x() + fontInfo.width(pChar)/2 + gradientOffsetX;
    float gradientStartY = baseline.y() - fontInfo.height()/2 - gradientOffsetY;
    float gradientEndX = gradientStartX - 2*gradientOffsetX;
    float gradientEndY = gradientStartY + 2*gradientOffsetY;

    QLinearGradient myGradient(gradientStartX,gradientStartY,gradientEndX, gradientEndY);
    myGradient.setStops(stops);
    mainFont.brush = myGradient;
    retVec.append(mainFont);

    if(ui->strokeCheckbox->isChecked())
    {
        PathAndBrush stroke;

        QPainterPathStroker stroker;
        stroker.setWidth(ui->strokeWidthBox->value());
        stroker.setJoinStyle(Qt::MiterJoin); // and other adjustments you need
        stroke.path = (stroker.createStroke(mainFont.path));

        QGradientStops strokeStops = ui->strokeGradientEditor->getGradientStops();

        float strokeGradientOffsetX = sin(fillAngle * ((2.0f * 3.14159265f)/360.0f)) * radius;
        float strokeGradientOffsetY = cos(fillAngle * ((2.0f * 3.14159265f)/360.0f)) * radius;

        float strokeGradientStartX = baseline.x() + fontInfo.width(pChar)/2 + strokeGradientOffsetX;
        float strokeGradientStartY = baseline.y() - fontInfo.height()/2 - strokeGradientOffsetY;
        float strokeGradientEndX = strokeGradientStartX - 2*strokeGradientOffsetX;
        float strokeGradientEndY = strokeGradientStartY + 2*strokeGradientOffsetY;

        QLinearGradient strokeGradient(strokeGradientStartX,strokeGradientStartY,strokeGradientEndX, strokeGradientEndY);
        strokeGradient.setStops(strokeStops);

        stroke.brush = strokeGradient;
        retVec.append(stroke);
    }

    return retVec;
}

void MainWindow::refreshPreview()
{
    QVector<QPainterPath> paths;
    QVector<QBrush> brushes;

    QFontMetrics fontInfo(_currentFont);
    QString characters[] = {
        "A","B","C","D","a","b","c","d","T","e","s","t",
    };

    float lastBaseLineX = 0;

    for(int i = 0; i < sizeof(characters)/sizeof(QString); ++i)
    {
        if(i != 0 && i % 4 == 0)
        {
            lastBaseLineX = 0;
        }

        QPointF baseline(lastBaseLineX, (1+i/4) * (fontInfo.height()+fontInfo.leading()));

        QVector<PathAndBrush> currentChar = getPathForCharacter(characters[i].at(0), baseline);

        for(int j = 0; j < currentChar.size(); ++j)
        {
            PathAndBrush currentPair = currentChar.at(j);

            paths.append(currentPair.path);
            brushes.append(currentPair.brush);
        }
        lastBaseLineX += fontInfo.width(characters[i].at(0));
    }

    ui->previewWindow->setTextBrushes(brushes);
    ui->previewWindow->setTextPaths(paths);

    ui->previewWindow->update();
}

void MainWindow::on_gradientAngleBox_valueChanged(double )
{
    refreshPreview();
}

void MainWindow::on_gradientAngleBox_editingFinished()
{
    refreshPreview();
}

void MainWindow::on_fontGradientEditor_gradientStopsChanged(const QGradientStops &stops)
{
    refreshPreview();
}

void MainWindow::on_dropShadowCheckbox_clicked()
{
    refreshPreview();
}

void MainWindow::on_strokeCheckbox_clicked()
{
    refreshPreview();
}

void MainWindow::on_dropShadowAngleBox_valueChanged(double )
{
    refreshPreview();
}

void MainWindow::on_dropShadowSizeBox_valueChanged(double )
{
    refreshPreview();
}

void MainWindow::on_shadowDistanceBox_valueChanged(double )
{
    refreshPreview();
}

void MainWindow::on_strokeWidthBox_valueChanged(double )
{
    refreshPreview();
}

void MainWindow::on_strokeGradientEditor_gradientStopsChanged(const QGradientStops &)
{
    refreshPreview();
}

void MainWindow::on_strokeGradientEditor_customContextMenuRequested(const QPoint &)
{

}

void MainWindow::on_exportButton_clicked()
{
    QString folder = QFileDialog::getExistingDirectory(this);
    QString characterset = ui->characterSetBox->text();
    QMap<QChar, QVector<PathAndBrush> > outputCollection;

    QFontMetrics fontInfo(_currentFont);


    float maxHeight = fontInfo.height();
    float maxWidth = fontInfo.maxWidth();

    for(int i = 0; i < characterset.length(); ++i)
    {
        QVector<PathAndBrush> current = getPathForCharacter(characterset.at(i), QPointF(0,(fontInfo.height()+fontInfo.leading())));
        outputCollection[characterset.at(i)] = current;

        QPainterPath compoundPath;
        for(int j = 0; j < current.size(); ++j)
        {
            compoundPath = compoundPath + current.at(j).path;
        }

        float compoundWidth = compoundPath.boundingRect().width();
        if(compoundWidth > maxWidth)
        {
            maxWidth = compoundWidth;
        }

        float compoundHeight = compoundPath.boundingRect().height();
        if(compoundHeight > maxHeight)
        {
            maxHeight = compoundHeight;
        }
    }

    for(int i = 0; i < characterset.length(); ++i)
    {
        QVector<PathAndBrush> current = outputCollection[characterset.at(i)];

        float pixmapHeight = maxHeight;
        float pixmapWidth = maxWidth;

        QPainterPath compoundPath;
        for(int j = 0; j < current.size(); ++j)
        {
            compoundPath = compoundPath + current.at(j).path;
        }

        float compoundWidth = compoundPath.boundingRect().width();
        if(compoundWidth < pixmapWidth)
        {
            pixmapWidth = compoundWidth;
        }

        QPixmap pixmap(pixmapWidth+2, pixmapHeight+2);
        pixmap.fill(Qt::transparent);
        QPainter painter(&pixmap);
        painter.translate(-compoundPath.boundingRect().left() + 1,-fontInfo.descent() + 1);
        painter.setBackground(Qt::transparent);
        painter.setRenderHint(QPainter::Antialiasing);

        painter.setPen(Qt::NoPen);

        for(int j = 0; j < current.size(); ++j)
        {
            PathAndBrush currentPair = current.at(j);
            painter.fillPath(currentPair.path, currentPair.brush);
        }

        quint16 q = characterset.at(i).unicode();
        QString path = folder + "/0x%04.png";

        path = path.arg(q,4,16);
        path = path.replace(" ", "0");
        pixmap.save(path, "PNG");
    }
    QString msgBoxmessage = "Successfully exported %1 images. Recommended kerning is: %2. Recommended leading is: %3";
    msgBoxmessage = msgBoxmessage.arg( QString("%1").arg(characterset.length()),  QString("%1").arg(maxWidth - fontInfo.maxWidth()), QString("%1").arg(fontInfo.leading()));
    QMessageBox::information(this, "Exportation Success",msgBoxmessage, QMessageBox::Ok);
}
