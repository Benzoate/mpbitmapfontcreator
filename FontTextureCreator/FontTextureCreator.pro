#-------------------------------------------------
#
# Project created by QtCreator 2012-03-07T16:57:51
#
#-------------------------------------------------

QT       += core gui

TARGET = FontTextureCreator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    hoverpoints.cpp \
    gradients.cpp \
    arthurwidgets.cpp \
    arthurstyle.cpp \
    pathstrokerenderer.cpp

HEADERS  += mainwindow.h \
    hoverpoints.h \
    gradients.h \
    arthurwidgets.h \
    arthurstyle.h \
    pathstrokerenderer.h

FORMS    += mainwindow.ui

RESOURCES += \
    shared.qrc
